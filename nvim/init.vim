" based on https://github.com/stlehmann/neovim-config/blob/master/init.vim

" Common tweaks
let mapleader=','
set nobackup
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set mouse=a

set number
set cursorline
set nowrap
set colorcolumn=120
set noswapfile
set splitbelow
set splitright
syntax on

" Remaps
inoremap jk <ESC>
nnoremap <Tab> :tabnex<CR>
nnoremap <S-Tab> :tabprev<CR>
nnoremap <C-T> :tabnew<CR>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <silent> <Leader>bd :Bclose<CR>

" " Copy to clipboard (press y to copy a line) 
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y
nnoremap  <leader>yy  "+yy

" " Paste from clipboard (press p to paste a line)
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" search settings
set ignorecase
set smartcase
set wildignore=*/venv/*,*/.git/*,*/site/*
