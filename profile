parse_git_branch() {
    # Show current branch for git
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
# Set up Prompt and PATH (as well as current branch)
export PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;33m\]\h\[\033[0;33m\] \w\[\033[00m\]:\[\033[01;31m\]$(parse_git_branch)\[\033[00m\]\$ '

# GPG TTY if GPG is installed
if ! command -v gpg 2>&1 1>/dev/null; [ "$?" -ne "0" ]; then
    export GPG_TTY=$(tty)
fi

if ! command -v /usr/local/go/bin/go 2>&1 1>/dev/null; [ "$?" -ne "0" ]; then
    export GOPATH=$HOME/gocode
    export PATH=$PATH:/usr/local/go/bin:/home/pi/gocode/bin
fi


# YOLO commit
#  for i in {1..10}; do yolocommit; done
alias yolocommit='git commit --allow-empty -m "$(curl -s http://whatthecommit.com/index.txt)"'
