GOPATH=/Users/nolim1t/src/go
GOROOT=/usr/local/go
PATH=$PATH:$GOROOT/bin:$GOPATH/bin:/usr/local/monero

PROMPT='%(?.%F{green}√.%F{red}?%?)%f %F{yellow}%n@%m %f %F{red}%1~%f %# '
